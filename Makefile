CFLAGS=-Wall -O3 -g -Wextra -Wno-unused-parameter
CXXFLAGS=$(CFLAGS)
OBJECTS=nameplate.o
BINARIES=nameplate

# Where our library resides. You mostly only need to change the
# RGB_LIB_DISTRIBUTION, this is where the library is checked out.
RGB_LIB_DISTRIBUTION=rpi-rgb-led-matrix
RGB_INCDIR=$(RGB_LIB_DISTRIBUTION)/include
RGB_LIBDIR=$(RGB_LIB_DISTRIBUTION)/lib
RGB_LIBRARY_NAME=rgbmatrix
RGB_LIBRARY=$(RGB_LIBDIR)/lib$(RGB_LIBRARY_NAME).a
LDFLAGS+=-L$(RGB_LIBDIR) -l$(RGB_LIBRARY_NAME) -lrt -lm -lpthread -lbluetooth
CFLAGS+=-I$(RGB_INCDIR) -Isimpleini

#include local includes/libraries
LOCAL_INCDIR=include
LOCAL_LIBDIR=lib
LDFLAGS+=-L$(LOCAL_LIBDIR)
CFLAGS+=-I$(LOCAL_INCDIR)

#nameplate : nameplate.o $(RGB_LIBRARY)
#	$(CXX) $< -o $@ $(LDFLAGS)
all : $(RGB_LIBRARY) $(BINARIES)

$(RGB_LIBRARY): FORCE
	$(MAKE) -C $(RGB_LIBDIR)

nameplate : nameplate.o $(RGB_LIBRARY)
	$(CXX) $< -o $@ $(LDFLAGS)


#nameplate : nameplate.o
# All the binaries that have the same name as the object file.q
.PHONY: clean
clean:
	rm -f $(OBJECTS) $(BINARIES)

FORCE:
.PHONY: FORCE
