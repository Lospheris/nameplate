/* File: nameplate.cpp
 * Date: 9 February 2019
 * Description: Do me later*
 *
 * Credit:
 * Deamon code: https://gist.github.com/alexdlaird/3100f8c7c96871c5b94e*
 *
 */

#include "led-matrix.h"
#include "graphics.h"
#include "threaded-canvas-manipulator.h"
#include "SimpleIni.h"

#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <errno.h>
//#include <libexplain/unlink.h>

// Welp, I guess we are doing this
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#define LOGNAME "NamePlate-Daemon"
#define SLEEP_INTERVAL 5

//using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;

volatile bool stop_received = false;
volatile bool reload_received = false;

class TextComponent {
public:
	TextComponent(std::string text, std::string color, std::string fontName, int x, int y) {
		initialize(text, color, fontName, x, y);
	}

	TextComponent() : TextComponent("", "FF0000", "fonts/7x13.bdf", 0, 10) {}

	bool isFontLoaded() {
		return fontLoaded_;
	}

	bool initialize(std::string text, std::string color, std::string fontName, int x, int y) {
		this->setText( text);
		this->setColor(color);
		this->setPosition(x, y);
		return this->setFont(fontName);
	}

	void setColor(std::string color) {
		color_ = rgb_matrix::Color(std::stoi(color.substr(0, 2), 0, 16),
		                           std::stoi(color.substr(2, 2), 0, 16),
		                           std::stoi(color.substr(4, 2), 0, 16));
	}

	void setPosition(int x, int y) {
		x_ = x;
		y_ = y;
	}

	void setYPosition(int y) {
		y_ = y;
	}

	void setXPosition(int x) {
		x_ = x;
	}

	void setText(std::string text) {
		text_ = text;
	}

	bool setFont(std::string font) {
		if (!font_.LoadFont(font.c_str())) {
			fprintf(stderr, "Couldn't load font%s\n", font.c_str());
			fontLoaded_ = false;
		} else {
			fontLoaded_ = true;
		}
		return fontLoaded_;
	}

	void draw(rgb_matrix::Canvas *canvas) {
		rgb_matrix::DrawText(canvas, font_, x_, y_, color_, text_.c_str());
	}

private:
	std::string text_;
	rgb_matrix::Color color_;
	rgb_matrix::Font font_;
	int x_;
	int y_;
	bool fontLoaded_;
};

class FrameComponent {
public:
	FrameComponent(std::string color, int x, int y) {
		initialize(color, x, y);
	}

	FrameComponent() : FrameComponent("FF0000", 0, 10) {}

	void initialize(std::string color, int x, int y) {
		this->setColor(color);
		this->setPosition(x, y);
		type_ = "pixel";
	}

	void setColor(std::string color) {
		color_ = rgb_matrix::Color(std::stoi(color.substr(0, 2), 0, 16),
		                           std::stoi(color.substr(2, 2), 0, 16),
		                           std::stoi(color.substr(4, 2), 0, 16));
	}

	void setPosition(int x, int y) {
		x_ = x;
		y_ = y;
	}

	void setYPosition(int y) {
		y_ = y;
	}

	void setXPosition(int x) {
		x_ = x;
	}

	void draw(rgb_matrix::Canvas *canvas) {
		canvas->SetPixel(x_, y_, color_.r, color_.g, color_.b);
	}

	std::string getType() {
		return type_;
	}

protected:
	rgb_matrix::Color color_;
	std::string type_;
	int x_;
	int y_;
};

class LineComponent : public FrameComponent {
public:
	LineComponent(int x0, int y0, int x1, int y1, std::string color) : FrameComponent(color, x0, y0) {
		setPoint1(x1, y1);
		type_ = "line";
	}

	LineComponent() : FrameComponent() {
		setPoint1(x_, y_);
		type_ = "line";
	}

	void initialize(std::string color, int x0, int y0, int x1, int y1) {
		this->setPoint0(x0, y0);
		this->setPoint1(x1, y1);
		this->setColor(color);
		type_ = "line";
	}

	void setPoint0(int x, int y) {
		x_ = x;
		y_ = y;
	}

	void setPoint1(int x, int y) {
		x1_ = x;
		y1_ = y;
	}

	void draw(rgb_matrix::Canvas *canvas) {
		rgb_matrix::DrawLine(canvas, x_, y_, x1_, y1_, color_);
	}

private:
	int x1_;
	int y1_;
};

class CircleComponent : public FrameComponent {
public:
	CircleComponent(int x, int y, int r, std::string color) : FrameComponent(color, x, y) {
		this->setRadius(r);
		type_ = "circle";
	}

	void initialize(int x, int y, int r, std::string color) {
		this->setPosition(x, y);
		this->setRadius(r);
		this->setColor(color);
		type_ = "cirlce";
	}

	void setRadius(int r) {
		r_ = r;
	}

	void draw(rgb_matrix::Canvas *canvas) {
		rgb_matrix::DrawCircle(canvas, x_, y_, r_, color_);
	}

private:
	int r_;
};

class NamePlate : public rgb_matrix::ThreadedCanvasManipulator {
public:
	NamePlate(RGBMatrix *m, CSimpleIni *config) : rgb_matrix::ThreadedCanvasManipulator(m), matrix_(m) {
		syslog(LOG_INFO, "NamePlate Constructor started!");
		off_screen_canvas_ = m->CreateFrameCanvas();
		syslog(LOG_INFO, "Frame canvas created, loading config");
		config_ = config;
		syslog(LOG_INFO, "Off screen canvas created, and config passed. Loading values from config file");
		std::string font_location = config_->GetValue("installation", "font_directory", "/");
		brightness_ = (uint8_t)config_->GetLongValue("global", "brightness", 255);
		font_location = config_->GetValue("installation", "font_directory", "");
		syslog(LOG_INFO, "Configuration information loaded, creating rankName");
		rankName_.initialize(config_->GetValue("rank_name", "text", "Shit's Broke"),
		                     config_->GetValue("rank_name", "color", "FF0000"),
		                     font_location + config_->GetValue("rank_name", "font", "fonts/7x13.bdf"),
		                     config_->GetLongValue("rank_name", "x", 0),
		                     config_->GetLongValue("rank_name", "y", 10));
		textDrawList_.push_back(&rankName_);
		
		syslog(LOG_INFO, "rankName Created, creating duty position");
		dutyPosition_.initialize(config_->GetValue("duty_position", "text", "Motherfucker"),
		                         config_->GetValue("duty_position", "color", "FF0000"),
		                         font_location + config_->GetValue("duty_position", "font", "fonts/7x13.bdf"),
		                         config_->GetLongValue("duty_position", "x", 0),
		                         config_->GetLongValue("duty_position", "y", 25));
		textDrawList_.push_back(&dutyPosition_);

		syslog(LOG_INFO, "dutyPosition Created, loading frame components");
		loadFrameComponents();
		syslog(LOG_INFO, "Frame components loaded, let's do the thing!");
	}

	void Run() {
		while (running()) {
			//usleep(100);
			off_screen_canvas_->SetBrightness(brightness_);
			off_screen_canvas_->Fill(0, 0, 0);

			for (auto text : textDrawList_) {
				text->draw(off_screen_canvas_);
			}
			for (auto component : frameDrawList_) {
				if (component->getType().compare( std::string("line") ) == 0) {
					lcPtr_ = static_cast<LineComponent*>(component);
					lcPtr_->draw(off_screen_canvas_);
				} else if (component->getType().compare( std::string("circle") ) == 0) {
					ccPtr_ = static_cast<CircleComponent*>(component);
					ccPtr_->draw(off_screen_canvas_);
				} else {
					component->draw(off_screen_canvas_);
				}
			}
			off_screen_canvas_ = matrix_->SwapOnVSync(off_screen_canvas_);
		}
	}

	void loadFrameComponents() {
		int componentCount = 0;
		componentCount = config_->GetLongValue("frame_global", "component_count", 0);
		for (int i = 0; i <= (componentCount - 1); i++) {
			std::string current_type = config_->GetValue((std::string("frame_component_") + std::to_string(i)).c_str(), "type", "invalid");
			if (current_type.compare( std::string("pixel")) == 0) {
				frameDrawList_.push_back(new FrameComponent(
										config_->GetValue((std::string("frame_component_") + std::to_string(i)).c_str(), "color", "FF0000"),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "x", 0),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "y", 0)));
			} else if (current_type.compare(std::string("line")) == 0) {
				frameDrawList_.push_back(new LineComponent(
										config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "x", 0),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "y", 0),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "x1", 0),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "y1", 0),
				                        config_->GetValue((std::string("frame_component_") + std::to_string(i)).c_str(), "color", "FF0000")));
			} else if (current_type.compare(std::string("circle")) == 0) {
				frameDrawList_.push_back(new CircleComponent(
										config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "x", 0),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "y", 0),
				                        config_->GetLongValue((std::string("frame_component_") + std::to_string(i)).c_str(), "radius", 1),
				                        config_->GetValue((std::string("frame_component_") + std::to_string(i)).c_str(), "color", "FF0000")));
			} else {
				// don't do anything here.
			}
		}
	}

	void deleteFrameComponents() {
		for (auto component : frameDrawList_) {
			delete(component);
		}
	}

	void Stop() {
		deleteFrameComponents();
		rgb_matrix::ThreadedCanvasManipulator::Stop();
	}

private:
	RGBMatrix *const matrix_;
	rgb_matrix::FrameCanvas *off_screen_canvas_;
	CSimpleIni *config_;
	std::string position_;
	TextComponent rankName_;
	TextComponent dutyPosition_;
	int frameComponentCount_ = 0;
	std::vector<TextComponent*> textDrawList_;
	std::vector<FrameComponent*> frameDrawList_;
	LineComponent* lcPtr_;
	CircleComponent* ccPtr_;
	uint8_t brightness_ = 0;
};

static void InterruptHandler(int signo) {
	if (signo == SIGUSR1) {
		std::cout << "SIGUSR1 Recieved, reload";
		reload_received = true;
	} else {
		std::cout << "SIGTERM received";
		stop_received = true;
	}
}

class Panel {

public:
	Panel() {
	
	}
	
	bool initialize(int *argc, char *argv[], std::string filename) {
		
		syslog(LOG_NOTICE, ("Opening config file: " + filename).c_str());
		error_ = config_.LoadFile(filename.c_str());
		if (error_ < 0) {
			syslog(LOG_ERR, "There was an error processing the config file.");
			//std::cout << "There was an error processing the config file." << std::endl;
			return false;
		}

		matrix_opts_.hardware_mapping = config_.GetValue("hardware", "hardware_mapping", "adafruit-hat");
		matrix_opts_.rows = config_.GetLongValue("hardware", "rows", 32);
		matrix_opts_.cols = config_.GetLongValue("hardware", "cols", 64);
		matrix_opts_.chain_length = config_.GetLongValue("hardware", "chain_length", 2);
		matrix_opts_.parallel = config_.GetLongValue("hardware", "parallel", 1);

		if (!ParseOptionsFromFlags(argc, &argv, &matrix_opts_, &runtime_opts_)) {
			std::cout << "Someting went wrong parsing the options" << std::endl;
			syslog(LOG_NOTICE, "Something went wrong parsing the options.");
			return false;
		}
		syslog(LOG_INFO, "Options configured! starting the matrix");
		matrix_ = CreateMatrixFromOptions(matrix_opts_, runtime_opts_);
		syslog(LOG_INFO, "Matrix Created! Creating nameplate.");
		nameplate_ = new NamePlate(matrix_, &config_);

		syslog(LOG_NOTICE, "Panel created Successfully!");

		return true;		
	}

	void destroy() {	
		delete nameplate_;
		delete matrix_;
	}

	void Start() {
	       nameplate_->Start();
	}

	void Stop() {
		nameplate_->Stop();
	}	

private:
	CSimpleIni config_;
	SI_Error error_ = SI_OK;
	rgb_matrix::RuntimeOptions runtime_opts_;
	RGBMatrix::Options matrix_opts_;
	RGBMatrix *matrix_;
	rgb_matrix::ThreadedCanvasManipulator *nameplate_;
	
};

bool fileExists(char* fileName) {
	struct stat fileInfo;
	return stat(fileName, &fileInfo) == 0;
}

int main (int argc, char *argv[]) {
	
	// Daemon Variables
	pid_t pid, sid;
	std::ofstream pid_file;
	Panel *panel = new Panel();
	std::string config_path = "/etc/nameplate/config.ini";
	
	// Forky magic
	pid = fork();

	// The parent process "should" be higher than 0. So if it is, GTFO
	if(pid > 0) {
		exit(EXIT_SUCCESS);
	// Something went wrong. Still GTFO, but this time a little sadder.
	} else if(pid < 0) {
		exit(EXIT_FAILURE);
	}

	// If execution gets here, it should be the child process (daemon).
	// Now it's time to make sure things are setup.
	// Apparently this needs to be set for the daemon to be able to access files. IDK why, that's 
	// just what I read.
	umask(0);

	// Since I can't really access STDIN/STDOUT/STDERR, it's time for some logging.
	openlog(LOGNAME, LOG_NOWAIT | LOG_PID, LOG_USER);
	syslog(LOG_NOTICE, "Successfully started NamePlate-Daemon");

	// Generate a session ID for the child process
	sid = setsid();
	
	//Ensure the SID is valid
	if (sid < 0) {
		// Log the failure
		syslog(LOG_ERR, "Could not generate session ID for child process");
		exit(EXIT_FAILURE);
	}
	syslog(LOG_NOTICE, "new Session ID acquired.");
	
	//Change the current working directory to a directory guaranteed to exist
	if((chdir("/")) < 0) {
		// Log failure and exit
		syslog(LOG_ERR, "Could not change working directory.");
		exit(EXIT_FAILURE);
	}
	
	// A daemon can't use the terminal, so now I need to close all the std file descriptors.
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	freopen("/home/pi/nameplate/dbg.log", "w", stdout);
	dup2(STDOUT_FILENO, STDERR_FILENO);


	close(STDIN_FILENO);	
	// Setup the signals so outside access is possible. This is how I intend to control the daemon
	signal(SIGTERM, InterruptHandler);
	signal(SIGINT, InterruptHandler);
	signal(SIGUSR1, InterruptHandler);
	
	// Get pid, and create pid file.
	if (fileExists("/tmp/nameplate.pid")) {
		syslog(LOG_ERR, "PID file already exists, so assuming the daemon is already running.");
		return -1;
	}

	pid_file.open("/tmp/nameplate.pid");
	if (pid_file.is_open()) {
		pid_file << getpid();
		syslog(LOG_NOTICE, "PID file created");
	} else {
		syslog(LOG_ERR, "Something went wrong creating the pidfile");
	}		

	syslog(LOG_NOTICE, "Daemon tasks taken care of. Start the panel.");
	panel->initialize(&argc, argv, config_path);
	panel->Start();
	syslog(LOG_NOTICE, "Panel initialized and started, entering main loop");

	while (!stop_received) {
		if (reload_received) {
			syslog(LOG_NOTICE, "Reloading the panel by request");
			panel->Stop();
			panel->destroy();
			if (!panel->initialize(&argc, argv, config_path)) {
				//std::cout << "Something went wrong reinitializing the panel" << std::endl;
				syslog(LOG_ERR, "Something went wrong reinitializing the panel");
				break;
			}
			panel->Start();
			reload_received = false;
			syslog(LOG_NOTICE, "Panel reloaded");
		}
		sleep(SLEEP_INTERVAL);
	}
	
	syslog(LOG_NOTICE, "Panel stopping");
	panel->Stop();
	panel->destroy();
	delete panel;
	pid_file.close();
	unlink("/tmp/nameplate.pid");
	if ( remove( "/tmp/nameplate.pid" ) ) {
		syslog(LOG_NOTICE, "There was an error deleting the PID file");
		std::cout << "Not sure why, but we couldn't delete the pid" << std::endl;
		perror("The following error occured while attempting to delete the pid file:");
	} else {
		syslog(LOG_NOTICE, "PID file deleted successfully");
	}
	syslog(LOG_NOTICE, "Panel stop. Memory release. Party on meat bags.");
	closelog();
	return 0;
}
